package com.example.demo;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import androidx.fragment.app.FragmentActivity;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.demo.data.Country;
import com.google.android.material.snackbar.Snackbar;

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    private String[] titles = {"France",
            "Germany",
            "Japan",
            "South africa",
            "Spain",
            "United states"};

    private String[] details = {"Item one details",
            "Item two details", "Item three details",
            "Item four details", "Item five details",
            "Item six details"};

    private int[] images = { R.drawable.ic_flag_of_france_320px,
            R.drawable.ic_flag_of_germany_320px,
            R.drawable.ic_flag_of_japan_320px,
            R.drawable.ic_flag_of_south_africa_320px,
            R.drawable.ic_flag_of_spain_320px,
            R.drawable.ic_flag_of_the_united_states_320px,
            };

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.card_layout, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(v);
        // Context c = view.getContext();
        //  view.setImageDrawable(c.getResources().getDrawable(
        //      c.getResources(). getIdentifier (uri, null , c.getPackageName())));
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
         viewHolder.itemTitle.setText(Country.countries[i].getName());
         viewHolder.itemDetail.setText(Country.countries[i].getCapital());
        //viewHolder.itemImage.setImageResource(images[i]);
        String uri = Country.countries[i].getImgUri();

         Context c = viewHolder.itemImage.getContext();
        viewHolder.itemImage.setImageDrawable(c.getResources().getDrawable(
              c.getResources(). getIdentifier (uri, null , c.getPackageName())));
      // viewHolder.itemTitle.setText(titles[i]);
      //  viewHolder.itemDetail.setText(details[i]);
       //viewHolder.itemImage.setImageResource(images[i]);
    }

    @Override
    public int getItemCount() {
        return titles.length;
    }

    static class ViewHolder extends RecyclerView.ViewHolder {
        ImageView itemImage;
        TextView itemTitle;
        TextView itemDetail;

        ViewHolder(View itemView) {
            super(itemView);
            itemImage = itemView.findViewById(R.id.item_image);
            itemTitle = itemView.findViewById(R.id.item_title);
            itemDetail = itemView.findViewById(R.id.item_detail);

            int position = getAdapterPosition();

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override public void onClick(View v) {

                    int position = getAdapterPosition();

                   Snackbar.make(v, "Click detected on chapter " + (position+1),
                       Snackbar.LENGTH_LONG)
                      .setAction("Action", null).show();


                    //// Implementation with bundle
            //        Bundle bundle = new Bundle();
              //     bundle.putInt("numChapter", position);
              //      Navigation.findNavController(v).navigate(R.id.action_ListFragment_to_DetailFragment, bundle);

              ListFragmentDirections.ActionListFragmentToDetailFragment action = ListFragmentDirections.actionListFragmentToDetailFragment();
                 action.setCountryId(position);
                  Navigation.findNavController(v).navigate(action);
                }
            });

        }
    }

}