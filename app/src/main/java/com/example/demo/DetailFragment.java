package com.example.demo;
/*
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

public class DetailFragment extends Fragment {

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }

    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }
}*/

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import com.example.demo.data.Country;

public class DetailFragment extends Fragment {
    public static final String TAG = "DetailFragment";
    TextView pays;
    ImageView imFlag;
    Button button;
    TextView icapital,ilangue, imonnaie, ipopulation,isuperfice;

    @Override
    public View onCreateView(
            LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState
    ) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_second, container, false);
    }


    public void onViewCreated(@NonNull View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        pays=view.findViewById(R.id.ipays);
        icapital=view.findViewById(R.id.icap);
        ilangue= view.findViewById(R.id.ilang);
        imonnaie =view.findViewById(R.id.imon);
        ipopulation=view.findViewById(R.id.ipop);
        isuperfice=view.findViewById(R.id.isup);
        imFlag=view.findViewById(R.id.flag);

        assert getArguments() != null;
        DetailFragmentArgs dArgs = DetailFragmentArgs.fromBundle(getArguments());
        int i =dArgs.getCountryId();

        String uri = Country.countries[i].getImgUri();
        Context c = imFlag.getContext();

        pays.setText(Country.countries[i].getName());
        icapital.setText(Country.countries[i].getCapital());
        ilangue.setText(Country.countries[i].getLanguage());
        imonnaie.setText(Country.countries[i].getCurrency());
        isuperfice.setText(Integer.toString(Country.countries[i].getArea()));
        ipopulation.setText(Integer.toString(Country.countries[i].getPopulation()));
        imFlag.setImageDrawable(c.getResources().getDrawable(c.getResources().getIdentifier(uri, null, c.getPackageName())));
        view.findViewById(R.id.button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                NavHostFragment.findNavController(DetailFragment.this)
                        .navigate(R.id.action_DetailFragment_to_ListFragment);
            }
        });
    }}